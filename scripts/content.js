'use strict';

// import * as browser from './browser-polyfill.js'


const endpoint = "http://localhost:4567/link";


const log = (message) =>
  console.log("laɪbnɪts - " + message);

const logE = (error) =>
  log("error: " + error);

browser.storage.local.set({"token": "bob"}).then(() => log("saved")).catch(logE);
