.PHONY: build lint clean run-firefox run-chromium

export PATH := $(PATH):./node_modules/.bin

version := $(shell jq -r .version manifest.json)
builder := web-ext build
linter := web-ext lint
runner := web-ext run

uri := http://localhost:4567/

content-script := scripts/content.js

extension-zip := web-ext-artifacts/laibnits-${version}.zip
extension-xpi := /tmp/laibnits-${version}.xpi


build: ${extension-xpi}

lint:
	${linter} -s .

clean:
	rm -f ${extension-zip} ${extension-xpi}

run-firefox:
	${runner) --target firefox-desktop --url ${uri}

run:
	${runner) --target chromium --url ${uri}

${extension-zip}: ${content-script}
	${builder} --overwrite-dest -s .

${extension-xpi}: ${extension-zip}
	cp ${extension-zip} ${extension-xpi}
