Repo containing an extension to try and work out how to use
[webextension-polyfil](https://github.com/mozilla/webextension-polyfill/releases).

I've included a `Makefile` to try and reduce the horrors of working with
javascript; hopefully it at least helps.
